#!/usr/bin/env python
# coding: utf-8

"""
author : theotime Perrichet

"""

class SimpleComplexCalculator:
    def __init__(self, complex_1, complex_2):
        self.complex_1 = complex_1
        self.complex_2 = complex_2

    def sum_complex(self):
        return [self.complex_1[0] + self.complex_2[0],self.complex_1[1] + self.complex_2[1]]

    def substract_complex(self):
        return [self.complex_1[0] - self.complex_2[0],self.complex_1[1] - self.complex_2[1]]

    def multiply_complex(self):
        return [self.complex_1[0] * self.complex_2[0] - self.complex_1[1] * self.complex_2[1], self.complex_1[0] * self.complex_2[1] + self.complex_1[1] * self.complex_2[0]]

    def divide_complex(self):
	 if ( self.complex_1[0]!=0 and self.complex_2[0]!=0 and self.complex_1[1]!=0 and self.complex_2[1]!=0):
		real = (self.complex_1[0] * self.complex_2[0] + self.complex_1[1] * self.complex_2[1]) / (self.complex_2[0]**2 + self.complex_2[1]**2)
	  	imag = (self.complex_1[1] * self.complex_2[0] - self.complex_1[0] * self.complex_2[1]) / (self.complex_2[0]**2 + self.complex_2[1]**2)
	  	return [real, imag]
	 else:
		return("vous avez essayé de diviser par zéro")



if __name__ == '__main__':
    MON_TEST = SimpleComplexCalculator([1,2], [1,3])
    print("Test de la classe avec [1,2] et [1,3]")
    print("somme : ", MON_TEST.sum_complex())
    print("soustraction : ", MON_TEST.substract_complex())
    print("multiplication : ",MON_TEST.multiply_complex())
    print("division : ", MON_TEST.divide_complex())


    MON_TEST2 = SimpleComplexCalculator([3,2], [1,6])
    print("Test de la classe avec [3,2] et [1,6]")
    print("somme : ", MON_TEST2.sum_complex())
    print("soustraction : ", MON_TEST2.substract_complex())
    print("multiplication : ", MON_TEST2.multiply_complex())
    print("division : ", MON_TEST2.divide_complex())

